module.exports = function (app, passport, fs, uuid) {
  
  app.get('/get_pages', function (req, res) {
    db.query('Select * from page order by updated_at DESC', function(err,result){
      if (err) {  
        res.status(200).send({ pages: result,error:err });
      }else{
        res.status(200).send({ pages: result,error:'' });
      }
    }); 
  });

  app.get('/get_page/:id', function (req, res){
      let query = "Select * from page where id = ('" + req.params.id + "')";
      
      db.query(query, function(err,result){
        res.status(200).send({ page: result,error:err });
      }); 
     
  }); 

  app.post('/save_page', function (req, res) {
     console.log(req.body)

     let query = "INSERT INTO `page`  (name,content,status) VALUES ('" + req.body.page_name + "','" +
      req.body.page_content + "','" + req.body.page_status + "')";
      db.query(query, (err, result) => {
        
        console.log(err,'err');

      if (err) {
        res.status(200).send({ status: false,msg:err });
      }else{
        res.status(200).send({ status: true });
      }
    }); 
  });

  app.post('/update_status', function (req, res) {
    var sql = "UPDATE `page` set status =?  WHERE id = ?";

    db.query(sql,[req.body.status,req.body.id], (err, result) => {
       
       console.log(err,'err');

     if (err) {
       res.status(200).send({ status: false,msg:err });
     }else{
       res.status(200).send({ status: true });
     }
   }); 
 });
  
  app.post('/update_page', function (req, res) {
    var sql = "UPDATE `page` set name =? , content =?, status =?  WHERE id = ?";
    let status = req.body.page_status == 'Active' ? 1 : 0;

    db.query(sql,[req.body.page_name,req.body.page_content,status,req.body.id], (err, result) => {
       
       console.log(err,'err');

     if (err) {
       res.status(200).send({ status: false,msg:err });
     }else{
       res.status(200).send({ status: true });
     }
   }); 
 });
};