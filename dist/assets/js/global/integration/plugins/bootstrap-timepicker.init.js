"use strict";
$.fn.timepicker.defaults = $.extend(true, {}, $.fn.timepicker.defaults, {
    icons: {
        up: 'la la-angle-up',
        down: 'la la-angle-down'
    }
});
//# sourceMappingURL=bootstrap-timepicker.init.js.map