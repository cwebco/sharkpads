"use strict";
var valGetParentContainer = function (element) {
    var element = $(element);
    if ($(element).closest('.form-group-sub').length > 0) {
        return $(element).closest('.form-group-sub');
    }
    else if ($(element).closest('.bootstrap-select').length > 0) {
        return $(element).closest('.bootstrap-select');
    }
    else {
        return $(element).closest('.form-group');
    }
};
jQuery.validator.setDefaults({
    errorElement: 'div',
    focusInvalid: false,
    ignore: "",
    errorPlacement: function (error, element) {
        var element = $(element);
        var group = valGetParentContainer(element);
        var help = group.find('.form-text');
        if (group.find('.valid-feedback, .invalid-feedback').length !== 0) {
            return;
        }
        element.addClass('is-invalid');
        error.addClass('invalid-feedback');
        if (help.length > 0) {
            help.before(error);
        }
        else {
            if (element.closest('.bootstrap-select').length > 0) { //Bootstrap select
                element.closest('.bootstrap-select').wrap('<div class="bootstrap-select-wrapper" />').after(error);
            }
            else if (element.closest('.input-group').length > 0) { //Bootstrap group
                element.after(error);
            }
            else { //Checkbox & radios
                if (element.is(':checkbox')) {
                    element.closest('.kt-checkbox').find('> span').after(error);
                }
                else {
                    element.after(error);
                }
            }
        }
    },
    highlight: function (element) {
        var group = valGetParentContainer(element);
        group.addClass('validate');
        group.addClass('is-invalid');
        $(element).removeClass('is-valid');
    },
    unhighlight: function (element) {
        var group = valGetParentContainer(element);
        group.removeClass('validate');
        group.removeClass('is-invalid');
        $(element).removeClass('is-invalid');
    },
    success: function (label, element) {
        var group = valGetParentContainer(element);
        group.removeClass('validate');
        group.find('.invalid-feedback').remove();
    }
});
jQuery.validator.addMethod("email", function (value, element) {
    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
        return true;
    }
    else {
        return false;
    }
}, "Please enter a valid Email.");
//# sourceMappingURL=jquery-validation.init.js.map