(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-views-pages-agents-agents-routing-module"],{

/***/ "./src/app/views/pages/agents/agents-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/views/pages/agents/agents-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: AgentsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgentsRoutingModule", function() { return AgentsRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var routes = [];
var AgentsRoutingModule = /** @class */ (function () {
    function AgentsRoutingModule() {
    }
    AgentsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AgentsRoutingModule);
    return AgentsRoutingModule;
}());



/***/ })

}]);
//# sourceMappingURL=app-views-pages-agents-agents-routing-module.js.map